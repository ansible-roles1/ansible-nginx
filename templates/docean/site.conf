server {
    listen                  443 ssl http2;
    listen                  [::]:443 ssl http2;
    server_name             {% for hostname in item.hostnames %}{{ hostname }} {% endfor %};

    {% if item.root %}
    set                     $base {{ item.root }};
    root                    {{ item.root }}{{ item.public }};
    {% endif %}

    # SSL
    ssl_certificate         /etc/letsencrypt/live/{{ item.hostnames[0] }}/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/{{ item.hostnames[0] }}/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/{{ item.hostnames[0] }}/chain.pem;

    # security
    include                 docean/security.conf;

    # additional config
    include docean/general.conf;
    {% if item.type != 'proxy-django' %}
    # Following rules don't work with Django...
    #

    # assets, media
    location ~* \.(?:css(\.map)?|js(\.map)?|jpe?g|png|gif|ico|cur|heic|webp|tiff?|mp3|m4a|aac|ogg|midi?|wav|mp4|mov|webm|mpe?g|avi|ogv|flv|wmv)$ {
      expires    7d;
      access_log off;
    }

    # svg, fonts
    location ~* \.(?:svgz?|ttf|ttc|otf|eot|woff2?)$ {
      add_header Access-Control-Allow-Origin "*";
      expires    7d;
      access_log off;
    }
    #
    {% endif %}

    {% set type = item.type %}
    {% if type == 'static' %}
      index                   index.html;
    {% endif %}

    {% if 'php' in type %}
      # index.php
      index                   index.php;

      # index.php fallback
      location / {
          try_files $uri $uri/ /index.php?$query_string;
      }

      # handle .php
      location ~ \.php$ {
          include docean/php_fastcgi.conf;
      }

      {% if 'bedrock' in type %}
        include docean/wordpress/bedrock.conf;
      {% endif %}

    {% endif %}

    {% if 'proxy' in type %}
      location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_redirect off;
        proxy_pass http://127.0.0.1:{{ item.port }};
      }

      {% if 'django' in type %}
      location /static {
        alias /sites/{{ item.name }}/static/;
      }

      location /media {
        alias /sites/{{ item.name }}/media/;
      }
      {% endif %}

    {% endif %}

    access_log /var/log/nginx/{{ item.name }}_access.log;
    error_log /var/log/nginx/{{ item.name }}_error.log;

}

# HTTP redirect
server {
    listen  80;
    listen  [::]:80;
    server_name             {% for hostname in item.hostnames %}{{ hostname }} {% endfor %};
    return 301 https://$server_name$request_uri;
    include docean/letsencrypt.conf;
}
