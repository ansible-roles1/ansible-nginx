# Nginx Ansible Role

Deploy nginx websites configurations.

**This role is just at its beginning...**

It works, but not from scratch yet. What you have to do manually:

- Generate **letsencrypt** certificates on host
- Change owners and rights on root folder if needed

Only **static** websites are currently deployables.

## from digital ocean

This role is inspired by https://www.digitalocean.com/community/tools/nginx
